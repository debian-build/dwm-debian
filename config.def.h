/* See LICENSE file for copyright and license details. */

/* appearance */
static const char *fonts[] = {
	"Terminus:size=10",
	"monospace:size=10"
};
static const char dmenufont[]       = "Terminus:size=10";
static const char normbordercolor[] = "#444444";
static const char normbgcolor[]     = "#222222";
static const char normfgcolor[]     = "#bbbbbb";
static const char selbordercolor[]  = "#005577";
static const char selbgcolor[]      = "#222222";
static const char selfgcolor[]      = "#eeeeee";
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, 0: display systray on the last monitor*/
static const int showsystray        = 1;        /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class                instance    title       tags mask     isfloating   monitor */
	{ "Gimp",               NULL,       NULL,       1 << 5,       1,           -1 },
	{ "Sublime_text",       NULL,       NULL,       1 << 0,       0,           -1 },
	{ "Google-chrome-beta", NULL,       NULL,       1 << 1,       0,           -1 },
	{ "Chromium",           NULL,       NULL,       1 << 1,       0,           -1 },
	{ "Firefox",            NULL,       NULL,       1 << 1,       0,           -1 },
	{ "skypeforlinux",      NULL,       NULL,       1 << 2,       0,           -1 },
	{ "Slack",              NULL,       NULL,       1 << 2,       0,           -1 },
	{ "Pcmanfm",            NULL,       NULL,       1 << 3,       0,           -1 },
	{ "discord",            NULL,       NULL,       1 << 5,       0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-fn", dmenufont, "-m", dmenumon, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbgcolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { "x-terminal-emulator", NULL };
static const char *slockcmd[] = { "slimlock", NULL };

static const char *scrcmd[]   = { "scrot", NULL };
static const char *scrucmd[]  = { "scrot", "-u", NULL };

static const char *vdowncmd[] = { "amixer", "-q", "set", "Master", "5%-", "unmute", NULL };
static const char *vupcmd[]   = { "amixer", "-q", "set", "Master", "5%+", "unmute", NULL };
static const char *vmutecmd[] = { "amixer", "-q", "set", "Master", "toggle", NULL };

static const char *playcmd[]  = { "mocp", "-G", NULL };
static const char *nextcmd[]  = { "mocp", "-f", NULL };
static const char *prevcmd[]  = { "mocp", "-r", NULL };

static Key keys[] = {
	/* modifier             key    function        argument */

	{ MODKEY,              113,    spawn,          {.v = vdowncmd } },
	{ MODKEY,              114,    spawn,          {.v = vupcmd } },
	{ MODKEY,              116,    spawn,          {.v = vmutecmd } },
	{ MODKEY,              111,    spawn,          {.v = playcmd } },

	{ ControlMask|Mod1Mask, 46,    spawn,          {.v = slockcmd } },

	{ MODKEY,               27,    spawn,          {.v = dmenucmd } }, // r
	{ MODKEY,               36,    spawn,          {.v = termcmd } }, // Return
	{ MODKEY,               56,    togglebar,      {0} },             // b
	{ MODKEY,               44,    focusstack,     {.i = +1 } },      // j
	{ MODKEY,               45,    focusstack,     {.i = -1 } },      // k
	{ Mod1Mask,             23,    focusstack,     {.i = -1 } },      // Tab
	{ MODKEY,               31,    incnmaster,     {.i = +1 } },      // i
	{ MODKEY,               40,    incnmaster,     {.i = -1 } },      // d
	{ MODKEY,               43,    setmfact,       {.f = -0.05} },    // h
	{ MODKEY,               46,    setmfact,       {.f = +0.05} },    // l
	{ MODKEY|ShiftMask,     36,    zoom,           {0} },             // Return
	{ MODKEY,               23,    view,           {0} },             // Tab
	{ Mod1Mask,             70,    killclient,     {0} },             // F4
	{ MODKEY,               28,    setlayout,      {.v = &layouts[0]} }, // t
	{ MODKEY,               41,    setlayout,      {.v = &layouts[1]} }, // f
	{ MODKEY,               65,    setlayout,      {0} },             // space
	{ MODKEY|ShiftMask,     65,    togglefloating, {0} },             // space
	// { MODKEY,               19,    view,           {.ui = ~0 } },     // 0
	// { MODKEY|ShiftMask,     19,    tag,            {.ui = ~0 } },     // 0
	{ MODKEY,               59,    focusmon,       {.i = -1 } },      // comma
	{ MODKEY,               60,    focusmon,       {.i = +1 } },      // period
	{ MODKEY|ShiftMask,     59,    tagmon,         {.i = -1 } },      // comma
	{ MODKEY|ShiftMask,     60,    tagmon,         {.i = +1 } },      // period
	TAGKEYS(                10,                    0)                 // 1
	TAGKEYS(                11,                    1)                 // 2
	TAGKEYS(                12,                    2)                 // 3
	TAGKEYS(                13,                    3)                 // 4
	TAGKEYS(                14,                    4)                 // 5
	TAGKEYS(                15,                    5)                 // 6
	TAGKEYS(                16,                    6)                 // 7
	TAGKEYS(                17,                    7)                 // 8
	TAGKEYS(                18,                    8)                 // 9
	TAGKEYS(                19,                    9)                 // 0
	{ MODKEY|ShiftMask,     24,    quit,           {0} },             // q
};

/* button definitions */
/* click can be ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[1]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
